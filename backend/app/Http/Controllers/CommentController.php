<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\User;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();
        foreach ($comments as $comment) {
            $user = User::find($comment->userId);
            $response[] = [
                'message' => $comment->message,
                'username' => $user->username,
                'date'=> $comment->created_at,
            ];
        }
        return $response;
    }

    public function comment_dates()
    {
        $comments = Comment::all();
        $array = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0,
            13 => 0,
            14 => 0,
            15 => 0,
            16 => 0,
            17 => 0,
            18 => 0,
            19 => 0,
            20 => 0,
            21 => 0,
            22 => 0,
            23 => 0,
            24 => 0,
            25 => 0,
            26 => 0,
            27 => 0,
            28 => 0,
            29 => 0,
            30 => 0,
            31 => 0,

        );

        foreach ($comments as $comment) {
            $i=intval(substr($comment->created_at, -11, 2));
            $array[$i]=$array[$i]+1;
        }

        for($a=1; $a<count($array); $a++){
            $response[] = [
                'd'=> $a,
                't'=>$array[$a],
            ];
        }

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'userId'=>'required'
        ]);
        $comment = new \App\Comment([
            'message' => $request->get('message'),
            'userId' => $request->get('userId')
        ]);
        $comment->save();

        if(!is_null($comment)) {
            return 'comment created';
        }
        else {
            return back()->with('error', 'Try again.');
        }
    }

}
