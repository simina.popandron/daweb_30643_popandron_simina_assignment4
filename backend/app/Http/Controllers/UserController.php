<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Session;

/**
 * Post
 *
 * @mixin Eloquent
 */

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return response()->json($users);
    }

    public function show(User $user)
    {
        return $user;
    }

    public function userRegister(Request $request) {
        $request->validate([
            'username'=>'required',
            'password'=>'required',
            'email'=>'required',
            'name' => 'required'
        ]);
        $user = new \App\User([
            'username' => $request->get('username'),
            'password' => $request->get('password'),
            'email' => $request->get('email'),
            'name' => $request->get('name'),
            'photo' => $request->get('photo'),
            'interests' => $request->get('interests')
        ]);
        $user->save();

        if(!is_null($user)) {
            return 'created';
        }
        else {
            return back()->with('error', 'Try again.');
        }
    }

    public function userLogin(Request $request) {
        $username = $request->get('username');
        $password = $request->get('password');
        $data = User::all()->where('username', $username)->first();
        if($data) {
            if ($password == $data->password) {
                Session::put ('name', $data->get('name'));
                Session::put ('username', $data->get('username'));
                Session::put ('login', TRUE);
            }
            else {
            }
        }
        else {
            return "not logged";
        }
        return response()->json($data);
    }

    public function findByID($id)
    {
        return User::all()->find($id);

    }

    public function updatebyID(Request $request, $id)
    {
        User::all()->find($id)->update($request->all());
    }

}
