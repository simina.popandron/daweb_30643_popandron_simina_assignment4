<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@userRegister');
Route::post('login', 'UserController@UserLogin');
Route::get('/findByID/{id}', 'UserController@findByID');
Route::put('/updatebyID/{id}', 'UserController@updatebyID');
Route::post('store', 'CommentController@store');
Route::get('comments', 'CommentController@index');
Route::get('dates', 'CommentController@comment_dates');
