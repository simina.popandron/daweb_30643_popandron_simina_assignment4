import axios from 'axios';

class UserAPI {
    addComment(comment) {
        return axios.post(`http://127.0.0.1:8000/api/store`, comment);
    }

    viewComment(){
        return axios.get('http://127.0.0.1:8000/api/comments')
    }

    commentDates(){
        return axios.get('http://127.0.0.1:8000/api/dates')
    }
}

export default new UserAPI();