import axios from 'axios';

class UserAPI {
    login(logindata) {
        return axios.post(`http://127.0.0.1:8000/api/login`, logindata);
    }

    register(registerdata) {
        return axios.post(`http://127.0.0.1:8000/api/register`, registerdata);
    }

    findUserByID(id){
        return axios.get(`http://127.0.0.1:8000/api/findByID/`+ id );
    }

    updateUser(updatedata, id){
        return axios.put('http://127.0.0.1:8000/api/updatebyID/'+id, updatedata);
    }

}

export default new UserAPI();