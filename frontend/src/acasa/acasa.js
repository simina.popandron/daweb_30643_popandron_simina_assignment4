import React from "react";
import {Button, Container, Jumbotron} from "reactstrap";

import BackgroundImg from '../commons/images/cover.png';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "700px",
    backgroundImage: `url(${BackgroundImg})`,
    padding:"0%"
};

class Acasa extends React.Component {
    render(){

        let content = {
            English: {
                f1: "An app born due to the high level of pollution caused by the increased number of cars that are daily going through the city of Cluj-Napoca.",
                f2: "We can save the city with every step taken, in a fun and useful way."
            },
            Romana: {
                f1: "O aplicatie nascuta datorita nivelului crescut de poluare cauzat de numarul crescut de masini care se afla zinlnic in trafic in Cluj-Napoca.",
                f2: "Putem salva orasul cu fiecare pas facut, intr-un mod distractiv si folositor pentru noi."
            }
        };
        localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);
        return(
                    <Jumbotron fluid style={backgroundStyle} className="container-fluid page_background_color full text-acasa">
                        <Container fluid>
                            <p className="text_acasa ">{content.f1}</p>
                            <p className="text_acasa ">{content.f2}</p>
                        </Container>
                    </Jumbotron>
        );
    }
}

export default Acasa;