import {GoogleMap} from '@react-google-maps/api';
import * as React from 'react';
/*global google*/
import  { compose, withProps, lifecycle } from 'recompose';
import {withScriptjs, withGoogleMap, DirectionsRenderer} from 'react-google-maps';


let currentLatLng = {
    lat: 0,
    lng: 0
};
class Contact extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentLatLng: {
                lat: 0,
                lng: 0
            },
            isMarkerShown: false
        }
    }
    showCurrentLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => {
                    currentLatLng.lat = position.coords.latitude;
                    currentLatLng.lng = position.coords.longitude;
                })
        }
    };
    componentDidMount() {
        this.showCurrentLocation()
    }

    render() {

        let content = {
            English: {
                ocupatie: "Student at UTCN, Facuty of Automation and Computers (Information Technology)",
                telefon: "Phone number: ",
                text: "For any question, please send an email to the address below and I will answer within 24 hours.",
                n:"Name",
                t:"Send",
                adresa:"Adress: "
            },
            Romana: {
                ocupatie: "Studenta UTCN, Facultatea de Automatica si Calculatoare (sectia TI) ",
                telefon: "Numar de telefon: ",
                text: "Pentru orice intrebare va rog sa trimiteti un mail la adresa de mai jos si va voi raspunde in maxim 24 de ore.",
                n:"Nume",
                t:"Trimite",
                adresa: "Adresa: "
            }
        };
        localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);

        const Map = compose(
            withProps({
                googleMapURL: `https://maps.googleapis.com/maps/api/js?key=AIzaSyDAMwtVJTWeRiOBgep2YAytM8IughFWGGY&&v=3.exp&libraries=geometry,drawing,places`,
                loadingElement: <div style={{ height: `400px` }} />,
                containerElement: <div style={{ width: `100%` }} />,
                mapElement: <div style={{height: `600px`, width: `100%` }}  />,
            }),
            withScriptjs,
            withGoogleMap,
            lifecycle({
                componentDidMount() {
                    const DirectionsService = new google.maps.DirectionsService();
                    DirectionsService.route({
                        origin: new google.maps.LatLng(currentLatLng.lat, currentLatLng.lng),
                        destination: new google.maps.LatLng(46.772443, 23.585240),
                        travelMode: google.maps.TravelMode.DRIVING,
                    }, (result, status) => {
                        if (status === google.maps.DirectionsStatus.OK) {
                            this.setState({
                                directions: {...result},
                                markers: true
                            })
                        } else {
                            console.error(`error fetching directions ${result}`);
                        }
                    });
                }
            })
        )(props =>
            <GoogleMap
                defaultZoom={3}
            >
                {props.directions && <DirectionsRenderer directions={props.directions} suppressMarkers={props.markers}/>}
            </GoogleMap>
        );
        return (
           <div className="container-fluid page_background_color full">
               <h1>Contact</h1>
               <div className="row">
                   <div className="col-sm-5">
                       <h3>Simina Diana Popandron</h3>
                       <h7>{content.ocupatie}</h7>
                       <br/><br/>
                       <p>{content.text}</p>
                       <br></br>
                       <p>{content.telefon}<a href={"https://www.whatsapp.com/"}> 0740180511 </a> </p>
                       <p>E-mail: <a href="https://www.google.com/intl/ro/gmail/about/"> simina.popandron@yahoo.com </a></p>
                       <p >Linked-in: <a href="https://www.linkedin.com/in/simina-popandron-849a45157/"> Simina Popandron </a> </p>
                       <p>{content.adresa}: UTCN Facultatea de Automatică și Calculatoare (str. George Barițiu 26-28) </p>
                   </div>
                   <div className="col-sm-5">
                       <div className="row">
                           <div className="col-sm-5 form-group">
                               <input className="form-control secondary_background_color" id="nume" name="nume" placeholder={content.n} type="text"
                                      required></input>
                           </div>
                           <div className="col-sm-6 form-group">
                               <input className="form-control " id="email" name="email" placeholder="Email"
                                      type="email" required></input>
                           </div>
                       </div>
                       <textarea className="form-control" id="text" name="text" placeholder="Text"
                                 rows="7"></textarea><br></br>
                       <div className="row">
                           <div className="col-sm-12 form-group">
                               <button className="btn btn-default btn-outline-default waves-effect" type="submit">{content.t}</button>
                           </div>
                       </div>
                   </div>
               </div>
               <Map/>
           </div>
        )
    }
}
export default Contact