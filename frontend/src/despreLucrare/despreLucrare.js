import React from "react";
import logo from '../commons/images/profile.png';
import cluj from '../commons/images/cluj.jpg';
import bus from '../commons/images/bus.jpg';
import walk from '../commons/images/walk.jpg';
import video from '../commons/images/video.mp4';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import CommentAPI from "../API/CommentAPI";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {Button} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import CanvasJSReact from '../asserts/canvasjs.react';
const CanvasJSChart = CanvasJSReact.CanvasJSChart;

class DespreLucrare extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userId: localStorage.getItem('id'),
            message:'',
            dates:[],
            data:[],
            y:[],
        };
    }

    handleAddComment = event => {
        let commentdata = {
            message: this.state.message,
            userId:this.state.userId,
            date:this.state.date
        };
        event.preventDefault();
        if(localStorage.getItem('id') != null) {
            CommentAPI.addComment(commentdata)
                .then(
                    window.location.reload(false)
                )
                .catch(e => console.log(e));
            window.location.reload(false);
        }else{
            alert("Please Log in");
        }
    };

    componentDidMount() {
        this.handleViewComment();
    }

    handleViewComment = () => {
        CommentAPI.viewComment()
            .then(response => this.setState({data:response.data},
            ))
            .catch(e => console.log(e));
        CommentAPI.commentDates()
            .then(response => this.setState({dates:response.data},
                                console.log(response.data),
                                //console.log(response.data.t)
                ))
                .catch(e => console.log(e));
    };

    comment_date(){
        for(let i=1; i<=this.state.dates.length; i++){
            this.state.y[i]=0;
        }
        let i=1;
        for(i=0; i<this.state.dates.length; i++){
            console.log(this.state.dates[i]);
            console.log(this.state.dates[21].t);
            this.state.y[0]=this.state.dates[0].t;
            this.state.y[1]=this.state.dates[1].t;
            this.state.y[2]=this.state.dates[2].t;
            this.state.y[3]=this.state.dates[3].t;
            this.state.y[4]=this.state.dates[4].t;
            this.state.y[5]=this.state.dates[5].t;
            this.state.y[6]=this.state.dates[6].t;
            this.state.y[7]=this.state.dates[7].t;
            this.state.y[8]=this.state.dates[8].t;
            this.state.y[9]=this.state.dates[9].t;
            this.state.y[10]=this.state.dates[10].t;
            this.state.y[11]=this.state.dates[11].t;
            this.state.y[12]=this.state.dates[12].t;
            this.state.y[13]=this.state.dates[13].t;
            this.state.y[14]=this.state.dates[14].t;
            this.state.y[15]=this.state.dates[15].t;
            this.state.y[16]=this.state.dates[16].t;
            this.state.y[17]=this.state.dates[17].t;
            this.state.y[18]=this.state.dates[18].t;
            this.state.y[19]=this.state.dates[19].t;
            this.state.y[20]=this.state.dates[20].t;
            this.state.y[21]=this.state.dates[21].t;
            this.state.y[22]=this.state.dates[22].t;
            this.state.y[23]=this.state.dates[23].t;
            this.state.y[24]=this.state.dates[24].t;
            this.state.y[25]=this.state.dates[25].t;
            this.state.y[26]=this.state.dates[26].t;
            this.state.y[27]=this.state.dates[27].t;
            this.state.y[28]=this.state.dates[28].t;
            this.state.y[29]=this.state.dates[29].t;
            //this.state.y[30]=this.state.dates[30].t;
        }
        console.log("y",  this.state.y);
    }

    render(){

        let content = {
            English: {
                about1:"Android application for monitoring the travel regime and acccounting the type of travel",
                about2:"The application will monitor the way the user moves (walking / public transport / car), and depending on the means used, points will be awarded. As with any game, the number of points results in levels, and at each level the player will be rewarded.",
                ob:"Objectives",
                o1:"➸ Encourage the people of Cluj to use the public transport, to the detriment of their personal cars or the available taxi / car shareing services.",
                o2:"➸ Motivate users to walk as much as possible, minimizing the use of any means of transport by awarding points that result in physical rewards.",
                o3:"➸ Air purification due to the decrease in the number of cars in traffic.",
                o4:"➸ Making a useful application for both the user and those around him."
            },
            Romana: {
                about1:"Aplicatie Android pentru monitorizarea regimului de deplasare si contorizarea tipului de deplasare",
                about2:"Aplicatia va monitoriza modul de deplasare a utilizatorului(mers pe jos/transport in comun/masina), iar in functie de mijlocul folosit se vor acorda puncte. Similar oricarui joc, numarul de puncte rezulta in nivele, iar la fiecare nivel utilizarotul va fi recompensat.",
                ob:"Obiective",
                o1:"➸ Indemnarea clujenilor de a folosi transportul in comun, in detrimentul masinilor personale sau a serviciilor de taxi/ car share-ing disponibile.",
                o2:"➸ Motivarea utilizatorilor de a merge pe jos cat mai mult, minimizand utilizarea oricarui mijloc de transport prin acordarea unor puncte care rezulta in premii fizice.",
                o3:"➸ Purificarea aerului datorita scaderii numarului de masini din trafic.",
                o4:"➸ Realizarea unei aplicatii folositoare atat pentru utilizator cat si pentru cei din jurul sau."
            }
        };
        localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);

        this.comment_date();

        const options = {
            animationEnabled: true,
            exportEnabled: true,
            theme: "dark2", //"light1", "dark1", "dark2"
            title:{
                text: "Graph for how many comments were given in each day of the month"
            },
            data: [{
                type: "column", //change type to bar, line, area, pie, etc
                indexLabelFontColor: "#5A5757",
                indexLabelPlacement: "outside",
                dataPoints: [
                    { x: 1, y: this.state.y[0] },
                    { x: 2, y: this.state.y[1] },
                    { x: 3, y: this.state.y[2] },
                    { x: 4, y: this.state.y[3] },
                    { x: 5, y: this.state.y[4] },
                    { x: 6, y: this.state.y[5] },
                    { x: 7, y: this.state.y[6] },
                    { x: 8, y: this.state.y[7] },
                    { x: 9, y: this.state.y[8] },
                    { x: 10, y: this.state.y[9] },
                    { x: 11, y: this.state.y[10] },
                    { x: 12, y: this.state.y[11] },
                    { x: 13, y: this.state.y[12] },
                    { x: 14, y: this.state.y[13] },
                    { x: 15, y: this.state.y[14] },
                    { x: 16, y: this.state.y[15] },
                    { x: 17, y: this.state.y[16] },
                    { x: 18, y: this.state.y[17] },
                    { x: 19, y: this.state.y[18] },
                    { x: 20, y: this.state.y[19] },
                    { x: 21, y: this.state.y[20] },
                    { x: 22, y: this.state.y[21] },
                    { x: 23, y: this.state.y[22] },
                    { x: 24, y: this.state.y[23] },
                    { x: 25, y: this.state.y[24] },
                    { x: 26, y: this.state.y[25] },
                    { x: 27, y: this.state.y[26] },
                    { x: 28, y: this.state.y[27] },
                    { x: 29, y: this.state.y[28] },
                    { x: 10, y: this.state.y[29] },
                ]
            }]
        };

        return(

            <div className="container-fluid page_background_color full">
                <div className="container text-center">
                    <div className="row" >
                        <div className="col-sm-5 col-md-6" >
                            <Carousel showThumbs={false} dynamicHeight>
                                <div>
                                    <img src={logo} />
                                </div>
                                <div>
                                    <img src={cluj} />
                                </div>
                                <div>
                                    <img src={bus} />
                                </div>
                                <div>
                                    <img src={walk} />
                                </div>
                                <div>
                                    <video width="500" height="500" controls >
                                        <source src={video} type="video/mp4"/>
                                    </video>
                                </div>
                            </Carousel>
                        </div>

                        <div className="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0">
                            <h4 className={"text_mare_about"}>{content.about1}</h4>
                            <br/>
                            <h9 className={"text_mic_about"}>{content.about2}</h9>
                        </div>
                    </div>

                    <br/>
                    <br/>

                    <h3 className={"text_mare_about"}>{content.ob}</h3>
                    <div className="row text_mic_about">
                        <div className="col-sm-3">
                            <p>{content.o1}</p>
                        </div>
                        <div className="col-sm-3">
                            <p>{content.o2}</p>
                        </div>
                        <div className="col-sm-3">
                            <p>{content.o3}</p>
                        </div>
                        <div className="col-sm-3">
                            <p>{content.o4}</p>
                        </div>
                    </div>
                </div>

                <h2 className="panel-heading" style={{color: 'black'}}>Comment Pannel: </h2><br/>
                <Form onSubmit={this.handleAddComment}>
                    <Form.Group as={Row} controlId="formHorizontalDescription">
                        <Col>
                            <textarea cols={80} rows={8} type="text" placeholder="Add a comment" value={this.state.message} onChange={e => this.setState({message:e.target.value})} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row}>
                        <Col>
                            <Button  color="primary"  type="submit" >Comment</Button>
                        </Col>
                    </Form.Group>
                </Form>

                <div className="row bootstrap snippets">
                    <div className="col-md-6 col-md-offset-2 col-sm-12">
                        <div className="comment-wrapper">
                            <div className="panel panel-info">
                                <h2 className="panel-heading" style={{color: 'black'}}>Comments: </h2><br/>
                                <div className="panel-body media-list clearfix pull-left ">
                                    {this.state.data.map(comment => {return(<div>
                                            <p>username: @{comment.username}</p> <p className="text-success">Comment: >>> {comment.message}</p>
                                            <hr className="hr" /><br/></div>
                                    )})}
                                </div></div></div></div></div>

                <div>
                    <br/><br/><br/>
                    <CanvasJSChart options = {options}/>
                </div>

            </div>
        );
    }
}
export default DespreLucrare;