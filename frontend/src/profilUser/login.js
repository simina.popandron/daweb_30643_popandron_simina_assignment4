import React, {useState} from 'react';
import FormGroup from 'react-bootstrap/FormGroup';
import FormControl from 'react-bootstrap/FormControl';
import {Button} from 'react-bootstrap';
import UserAPI from '../API/UserAPI';
import {getIsLoggedInFromLocalStorage, setIsLoggedInLocalStorage} from '../Helpers';
import {Redirect} from "react-router";


export default function Login() {
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    let redirect;

    const isLoggedIn = getIsLoggedInFromLocalStorage();

    if (isLoggedIn === 'true') {
        redirect = <Redirect push to="/"/>
    }
    else{
        redirect='';
    }

    function handleLoginSubmit(event) {
        let logindata = {
            username: username,
            password: password
        }

        UserAPI.login(logindata)
            .then(response => {
                setIsLoggedInLocalStorage('true');
                localStorage.setItem('id', response.data.id);
                //console.log(response.data.id);
                console.log(localStorage.getItem('id'));
                window.location.reload(false);
            })
            .catch(error => {
                alert('Emailul si parola nu corespund');
            });
        console.log("whatever");
        console.log(localStorage.getItem('id'));
        event.preventDefault();
    }
    function validateForm() {
        return password.length > 0 && username.length > 0;
    }
        return(
            <div className="container-fluid page_background_color full text-center container portfolio" >
                <div className="wrapper fadeInDown">
                    <div id="formContent">
                        <form onSubmit={handleLoginSubmit}>
                            <FormGroup controlId="username" bssize="large">
                                <label>Username</label>
                                <FormControl
                                    autoFocus
                                    type="username"
                                    value={username}
                                    onChange={e => setUsername(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup controlId="password" bssize="large">
                                <label>Parola</label>
                                <FormControl
                                    type="password"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                            </FormGroup>
                            <Button block bssize="large" disabled={!validateForm()} type="submit" >
                            Login
                        </Button>
                        </form>
                        {redirect}
                    </div>
                </div>
            </div>
        );
    }

