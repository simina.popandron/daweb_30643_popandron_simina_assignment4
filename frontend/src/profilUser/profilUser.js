import React from "react";
import UserAPI from '../API/UserAPI';

class ProfilUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: localStorage.getItem('id'),
            data: [],
            username: '',
            name: '',
            email: '',
            photo: null,
            password:'',
            interests:''
        };
    }

    componentDidMount() {
        if (localStorage.getItem('id') != null) {
            this.handleProfilUser();
        }
    }

    handleProfilUser=()=>{
        UserAPI.findUserByID(this.state.id)
            .then(response => {
                this.setState({data:response.data});
            })
            .catch(error => {
                alert('User Data not foud');
            });
    }
        render(){
            if (localStorage.getItem('id') != null) {
                return (
                    <div className="container-fluid page_background_color full text-center container portfolio">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 img">
                                    <img
                                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvzOpl3-kqfNbPcA_u_qEZcSuvu5Je4Ce_FkTMMjxhB-J1wWin-Q"
                                        alt="" className="img-rounded"/>
                                </div>
                                <div className="col-md-6 details">
                                    <blockquote>
                                        <h5>NAME: {this.state.data.name}</h5>
                                        <small><cite title="Source Title">
                                            <i className="icon-map-marker"></i></cite></small>
                                    </blockquote>
                                    <p>
                                        E-MAIL: {this.state.data.email}<br></br>
                                        USERNAME: {this.state.data.username}<br></br>
                                        INTERESTS: {this.state.data.interests}<br></br>

                                    </p>


                                </div>
                                <div className="col-md-8">
                                    <button id="cancel" name="edit" className="w3-button w3-border w3-hover-teal">
                                        <a href="/updateUserInfo" className="w3-button w3-black">Edit Profile</a>
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
            else{
                return(<div>Page not found</div>);
            }
        }

}

export default ProfilUser;