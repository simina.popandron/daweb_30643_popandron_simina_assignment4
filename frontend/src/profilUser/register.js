import React, {useState} from 'react';
import FormGroup from 'react-bootstrap/FormGroup';
import FormControl from 'react-bootstrap/FormControl';
import {Button} from 'react-bootstrap';
import UserAPI from '../API/UserAPI';
import Row from 'react-bootstrap/Row';
import FormLabel from 'react-bootstrap/FormLabel';
import Col from 'react-bootstrap/Col';
import FormCheck from 'react-bootstrap/FormCheck';

export default function Update() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [username, setUsername] = useState('');
    const [photo, setPhoto] = useState('');
    const [interests, setInterests] = useState('');

    // console.log(id);
    function handleRegisterSubmit(event) {
        let registerdata = {
            email: email,
            password: password,
            name: name,
            username: username,
            interests: interests
        }

        UserAPI.register(registerdata)
            .catch(error => {
                alert('Inregistrarea este oprită momentan');
            });

        console.log(registerdata);
    }
        return(
            <div className="container-fluid page_background_color full text-center container portfolio" >
                <div className="wrapper fadeInDown">
                    <div id="formContent">
                        <br></br>
                        <div className="fadeIn first">
                            <h5>Welcome! If you want to register to out page please enter the following data: </h5>
                        </div>
                        <form onSubmit={handleRegisterSubmit}>
                            <FormGroup controlId="name" bssize="large" className="fadeIn second">
                                <label>Name</label>
                                <FormControl
                                    autoFocus
                                    value={name}
                                    onChange={e => setName(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup controlId="emailRegister" bssize="large" className="fadeIn second">
                                <label>Email</label>
                                <FormControl
                                    autoFocus
                                    type="email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                />
                            </FormGroup>

                            <FormGroup controlId="username" bssize="large" className="fadeIn second">
                                <label>Username</label>
                                <FormControl
                                    autoFocus
                                    value={username}
                                    onChange={e => setUsername(e.target.value)}
                                />
                            </FormGroup>

                            <FormGroup controlId="passwordRegister" bssize="large"className="fadeIn third">
                                <label>Parola</label>
                                <FormControl
                                    type="password"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                            </FormGroup>

                            <fieldset>
                                <FormGroup as={Row}>
                                    <FormLabel as="legend" column sm={2}>
                                        Domeniu de interes:
                                    </FormLabel>
                                    <Col sm={10}>
                                        <FormCheck
                                            type="radio"
                                            label="Android"
                                            name="Android"
                                            id="formHorizontalRadios1"
                                            onChange={e => setInterests(e.target.name)}
                                        />
                                        <FormCheck
                                            type="radio"
                                            label="Ecologie"
                                            name="Ecologie"
                                            id="formHorizontalRadios2"
                                            onChange={e => setInterests(e.target.name)}
                                        />
                                        <FormCheck
                                            type="radio"
                                            label="Cluj-Napoca"
                                            name="Cluj-Napoca"
                                            id="formHorizontalRadios3"
                                            onChange={e => setInterests(e.target.name)}
                                        />
                                    </Col>
                                </FormGroup>
                            </fieldset>

                            <Button block bssize="large"  type="submit" className="fadeIn fourth">
                                Inregistrare
                            </Button>
                        </form>
                    </div>
                </div>
            </div>
        );


}